package com.dhy.mq;
import cn.hutool.extra.spring.SpringUtil;
import com.delayTask.DelayTaskEvent;
import com.delayTask.DelayTaskMain;
import com.delayTask.DelayTaskQueue;
import com.delayTask.domain.OrderDelayFactory;
import com.delayTask.rabbitmq.MqDelayQueueAdvanced;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.concurrent.TimeUnit;

/**
 * @author 大忽悠
 * @create 2022/9/19 10:34
 */
@SpringBootTest(classes = DelayTaskMain.class)
public class MqDelayQueueAdvancedTest {

    @Test
    public void test() throws InterruptedException {
        DelayTaskQueue mqDelayQueue = SpringUtil.getBean(MqDelayQueueAdvanced.class);
        DelayTaskEvent orderDelay =  OrderDelayFactory.newOrderDelay("大忽悠", "小风扇", 13.4, 15L);
        DelayTaskEvent orderDelay1 = OrderDelayFactory.newOrderDelay("小朋友", "冰箱", 3000.0, 3L);
        mqDelayQueue.produce(orderDelay);
        mqDelayQueue.produce(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        mqDelayQueue.cancel(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(30L));
    }
}
