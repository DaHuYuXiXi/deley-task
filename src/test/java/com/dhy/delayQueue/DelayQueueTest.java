package com.dhy.delayQueue;


import cn.hutool.extra.spring.SpringUtil;
import com.delayTask.DelayTaskEvent;
import com.delayTask.DelayTaskQueue;
import com.delayTask.delayQueue.DelayQueue;
import com.delayTask.domain.OrderDelayFactory;
import com.delayTask.domain.OrderDelayEvent;
import com.delayTask.rabbitmq.MqDelayQueueAdvanced;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

@Slf4j
public class DelayQueueTest {
    /**
     * 测试下单
     */
    @Test
    public void testOrder() throws InterruptedException {
        DelayTaskQueue mqDelayQueue = new DelayQueue();
        DelayTaskEvent orderDelay = OrderDelayFactory.newOrderDelay("大忽悠", "小风扇", 13.4, 15L);
        DelayTaskEvent orderDelay1 = OrderDelayFactory.newOrderDelay("小朋友", "冰箱", 3000.0, 3L);
        mqDelayQueue.produce(orderDelay);
        mqDelayQueue.produce(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        mqDelayQueue.cancel(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(30L));
    }


}
