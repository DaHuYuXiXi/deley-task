package com.dhy.redis;

import cn.hutool.extra.spring.SpringUtil;
import com.delayTask.DelayTaskEvent;
import com.delayTask.DelayTaskMain;
import com.delayTask.DelayTaskQueue;
import com.delayTask.domain.OrderDelayFactory;
import com.delayTask.rabbitmq.MqDelayQueueAdvanced;
import com.delayTask.zset.RedisOrderDelayQueue;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;


import java.util.concurrent.TimeUnit;

/**
 * @author 大忽悠
 * @create 2022/9/18 19:44
 */
@SpringBootTest(classes = DelayTaskMain.class)
public class RedisZSetTest {

    @Test
    public void testZSet() throws InterruptedException {
        DelayTaskQueue mqDelayQueue = SpringUtil.getBean(RedisOrderDelayQueue.class);
        DelayTaskEvent orderDelay =  OrderDelayFactory.newOrderDelay("大忽悠", "小风扇", 13.4, 15L);
        DelayTaskEvent orderDelay1 = OrderDelayFactory.newOrderDelay("小朋友", "冰箱", 3000.0, 3L);
        mqDelayQueue.produce(orderDelay);
        mqDelayQueue.produce(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        mqDelayQueue.cancel(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(30L));
    }

}
