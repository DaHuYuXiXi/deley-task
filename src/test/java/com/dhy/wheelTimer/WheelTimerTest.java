package com.dhy.wheelTimer;

import cn.hutool.extra.spring.SpringUtil;
import com.delayTask.DelayTaskEvent;
import com.delayTask.DelayTaskQueue;
import com.delayTask.domain.OrderDelayEvent;
import com.delayTask.domain.OrderDelayFactory;
import com.delayTask.wheelTimer.WheelTimerDelayQueue;
import com.delayTask.zset.RedisOrderDelayQueue;
import io.netty.util.Timeout;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * @author 大忽悠
 * @create 2022/9/17 17:19
 */
public class WheelTimerTest {

    @Test
    public void testWheelTimer() throws InterruptedException {
        DelayTaskQueue mqDelayQueue = new WheelTimerDelayQueue(100L);
        DelayTaskEvent orderDelay =  OrderDelayFactory.newOrderDelay("大忽悠", "小风扇", 13.4, 15L);
        DelayTaskEvent orderDelay1 = OrderDelayFactory.newOrderDelay("小朋友", "冰箱", 3000.0, 3L);
        mqDelayQueue.produce(orderDelay);
        mqDelayQueue.produce(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(5));
        mqDelayQueue.cancel(orderDelay1);

        Thread.sleep(TimeUnit.SECONDS.toMillis(30L));
    }
}
